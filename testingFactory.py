#!/usr/bin/python3

from testing.test_LSTM_DI import *

class TestFactory:

    @staticmethod
    def instance(sess, options):
        test = options.get('experiment','test')
        if test == "test_LSTM_DI":
            return TestLSTM_DI(sess, options)
