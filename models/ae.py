import tensorflow as tf
import numpy as np
import math

from models.model import BaseModel
from utils.ops import *

class ModelAE(BaseModel):
    def __init__(self, options):
        self.options = options

        self.d_bn1 = batch_norm(name='d_bn1')
        self.d_bn2 = batch_norm(name='d_bn2')
        self.d_bn3 = batch_norm(name='d_bn3')

        self.g_bn0 = batch_norm(name='g_bn0')
        self.g_bn1 = batch_norm(name='g_bn1')
        self.g_bn2 = batch_norm(name='g_bn2')
        self.g_bn3 = batch_norm(name='g_bn3')

    def build(self, imInput, desire=None):
        with tf.variable_scope("autoencoder") as scope:
            s_h, s_w = self.options.height, self.options.width
            s_h2, s_w2 = self.conv_out_size_same(s_h, 2), self.conv_out_size_same(s_w, 2)
            s_h4, s_w4 = self.conv_out_size_same(s_h2, 2), self.conv_out_size_same(s_w2, 2)
            s_h8, s_w8 = self.conv_out_size_same(s_h4, 2), self.conv_out_size_same(s_w4, 2)
            s_h16, s_w16 = self.conv_out_size_same(s_h8, 2), self.conv_out_size_same(s_w8, 2)

            #self.makeImageSummary("inputImage", imInput)
            e0 = lrelu(conv2d(imInput, self.options.df_dim, name='e0_conv'))
            e1 = lrelu(self.d_bn1(conv2d(e0, self.options.df_dim*2, name='e1_conv')))
            e2 = lrelu(self.d_bn2(conv2d(e1, self.options.df_dim*4, name='e2_conv')))
            e3 = lrelu(self.d_bn3(conv2d(e2, self.options.df_dim*8, name='e3_conv')))
            d4 = tf.nn.relu(self.g_bn0(deconv2d(e3, [self.options.batch_size, s_h8, s_w8, self.options.gf_dim*4], name='d4_deconv', with_w=False)))
            d3 = tf.nn.relu(self.g_bn1(deconv2d(d4, [self.options.batch_size, s_h4, s_w4, self.options.gf_dim*2], name='d3_deconv', with_w=False)))
            d2 = tf.nn.relu(self.g_bn2(deconv2d(d3, [self.options.batch_size, s_h2, s_w2, self.options.gf_dim*1], name='d2_deconv', with_w=False)))
            d1 = deconv2d(d2, [self.options.batch_size, s_h, s_w, self.options.channels], name='d1_deconv', with_w=False)
            imOutput = d1 #tf.nn.tanh(d1)
            #self.makeImageSummary("outputImage", imOutput)
            #self.makeImageSummary("desireImage", desire)
            return imOutput
