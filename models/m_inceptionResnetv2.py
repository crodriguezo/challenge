
import os
import numpy as np
import tensorflow as tf

from models.tfslim.research.slim.nets import inception
from models.model import BaseModel
from tensorflow.contrib import slim

class ModelInceptionResnetV2(BaseModel):
    def __init__(self, config):
        self.config = config
        self.checkpoint_exclude_scopes=["DI/InceptionResnetV2/Logits", "DI/InceptionResnetV2/AuxLogits"]

    def build(self, imInput):
        # Create the model, use the default arg scope to configure the batch norm parameters.
        train_stage = False
        dkp = float(self.config.get('InceptionResnetV2','dropout_keep'))
        if self.config.get('experiment','stage') == 'test':
            train_stage = False
            dkp = 1.
        with slim.arg_scope(inception.inception_resnet_v2_arg_scope()):
            logits, _ = inception.inception_resnet_v2(imInput, num_classes=int(self.config.get('experiment','classes')), is_training=train_stage, create_aux_logits=False, dropout_keep_prob=dkp)
            return logits, _

    def variablesToLoad(self, rVars=None):
        """Returns a function run by the chief worker to warm-start the training."""

        exclusions = [scope.strip() for scope in self.checkpoint_exclude_scopes]

        if rVars == None:
            variables_to_restore = []
            for var in slim.get_model_variables():
                excluded = False
                for exclusion in exclusions:
                    if var.op.name.startswith(exclusion):
                        excluded = True
                        break
                if not excluded:
                    variables_to_restore.append(var)

            return variables_to_restore
        else:
            variables_to_restore = []
            for var in rVars:
                excluded = False
                for exclusion in exclusions:
                    if var.op.name.startswith(exclusion):
                        excluded = True
                        break
                if not excluded:
                    variables_to_restore.append(var)

            return variables_to_restore
