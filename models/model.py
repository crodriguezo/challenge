#!/usr/bin/python3
import tensorflow as tf
import numpy as np
import math

class BaseModel(object):

    def conv_out_size_same(self, size, stride):
        return int(math.ceil(float(size) / float(stride)))

    def build(self):
        raise NotImplementedError

    def makeImageSummary(self, tag, image):
        with tf.name_scope("imageSummary"):
            blockSize = self.options.visBlockSize
            imageSlice = tf.slice(image[:blockSize**2],[0,0,0,0],[blockSize**2,-1,-1,-1])
            imageOne = tf.batch_to_space(imageSlice,crops=[[0,0],[0,0]],block_size=blockSize)
            imagePermute = tf.reshape(imageOne,[self.options.height,blockSize,self.options.width,blockSize,3])
            imageTransp = tf.transpose(imagePermute,[1,0,3,2,4])
            imageBlocks = tf.reshape(imageTransp,[1,self.options.height * blockSize,self.options.width * blockSize,3])
            tf.summary.image(tag,imageBlocks)
