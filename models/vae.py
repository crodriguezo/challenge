import tensorflow as tf
import numpy as np
import math

from models.model import BaseModel
from utils.ops import *

class ModelVAE(BaseModel):
    def __init__(self, options):
        self.options = options
        
        self.d_bn1 = batch_norm(name='d_bn1')
        self.d_bn2 = batch_norm(name='d_bn2')
        self.d_bn3 = batch_norm(name='d_bn3')

        self.g_bn0 = batch_norm(name='g_bn0')
        self.g_bn1 = batch_norm(name='g_bn1')
        self.g_bn2 = batch_norm(name='g_bn2')
        self.g_bn3 = batch_norm(name='g_bn3')
        
    def build(self, imInput, desire, isTraining = True, Reuse=False):
        with tf.variable_scope("vae") as scope:
            self.makeImageSummary("inputImage", imInput)
            # noises
            mu, sigma = self.encoder(imInput, is_training=isTraining, reuse=Reuse)      
            z = mu + sigma * tf.random_normal(tf.shape(mu), 0, 1, dtype=tf.float32)
            imOutput = self.decoder(z, is_training=isTraining, reuse=Reuse)
            imOutput = tf.clip_by_value(imOutput, 1e-8, 1 - 1e-8)
            self.makeImageSummary("outputImage", imOutput)
            self.makeImageSummary("desireImage", desire)
            return imOutput, mu, sigma

    # Gaussian Encoder
    def encoder(self, x, is_training=True, reuse=False):
        # Network Architecture is exactly same as in infoGAN (https://arxiv.org/abs/1606.03657)
        # Architecture : (64)4c2s-(128)4c2s_BL-FC1024_BL-FC62*4
        with tf.variable_scope("encoder", reuse=reuse):

            e0 = lrelu(conv2d(x, self.options.df_dim, name='e0_conv'))
            e1 = lrelu(self.d_bn1(conv2d(e0, self.options.df_dim*2, name='e1_conv')))
            e2 = lrelu(self.d_bn2(conv2d(e1, self.options.df_dim*4, name='e2_conv')))
            e3 = lrelu(self.d_bn3(conv2d(e2, self.options.df_dim*8, name='e3_conv')))
            net = tf.reshape(e3, [self.options.batch_size, -1])
            net = lrelu(bn(linear(net, 1024, scope='en_fc3'), is_training=is_training, scope='en_bn3'))
            gaussian_params = linear(net, 2 * self.options.z_dim, scope='en_fc4')
            # The mean parameter is unconstrained
            mean = gaussian_params[:, :self.options.z_dim]
            # The standard deviation must be positive. Parametrize with a softplus and
            # add a small epsilon for numerical stability
            stddev = 1e-6 + tf.nn.softplus(gaussian_params[:, self.options.z_dim:])
            return mean, stddev
        

    # Bernoulli decoder
    def decoder(self, z, is_training=True, reuse=False):
        # Network Architecture is exactly same as in infoGAN (https://arxiv.org/abs/1606.03657)
        # Architecture : FC1024_BR-FC7x7x128_BR-(64)4dc2s_BR-(1)4dc2s_S
        with tf.variable_scope("decoder", reuse=reuse):

            s_h, s_w = self.options.height, self.options.width
            s_h2, s_w2 = self.conv_out_size_same(s_h, 2), self.conv_out_size_same(s_w, 2)
            s_h4, s_w4 = self.conv_out_size_same(s_h2, 2), self.conv_out_size_same(s_w2, 2)
            s_h8, s_w8 = self.conv_out_size_same(s_h4, 2), self.conv_out_size_same(s_w4, 2)
            s_h16, s_w16 = self.conv_out_size_same(s_h8, 2), self.conv_out_size_same(s_w8, 2)
            
            net = tf.nn.relu(bn(linear(z, 1024, scope='de_fc1'), is_training=is_training, scope='de_bn1'))
            net = tf.nn.relu(bn(linear(net, 128 * 7 * 7, scope='de_fc2'), is_training=is_training, scope='de_bn2'))
            net = tf.reshape(net, [self.options.batch_size, 7, 7, 128])
            d4 = tf.nn.relu(self.g_bn0(deconv2d(net, [self.options.batch_size, s_h8, s_w8, self.options.gf_dim*4], name='d4_deconv', with_w=False)))
            d3 = tf.nn.relu(self.g_bn1(deconv2d(d4, [self.options.batch_size, s_h4, s_w4, self.options.gf_dim*2], name='d3_deconv', with_w=False)))
            d2 = tf.nn.relu(self.g_bn2(deconv2d(d3, [self.options.batch_size, s_h2, s_w2, self.options.gf_dim*1], name='d2_deconv', with_w=False)))
            d1 = deconv2d(d2, [self.options.batch_size, s_h, s_w, self.options.channels], name='d1_deconv', with_w=False)
            #imOutput = tf.nn.tanh(d1)
            return d1 #imOutput
