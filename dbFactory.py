#!/usr/bin/python3

from datasets.THUMOS import *
from datasets.JHMDB import *
from datasets.CHARADES import *

class DBFactory:

    @staticmethod
    def instance(config):
        dataset = config.get('experiment','dataset')
        if dataset == 'THUMOS':
            ret = THUMOS(config)
            return ret
        elif dataset == "JHMDB":
            ret = JHMDB(config)
            return ret
        elif dataset == "CHARADES":
            ret = CHARADES(config)
            return ret
