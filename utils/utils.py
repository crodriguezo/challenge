from __future__ import division
import math
import json
import random
import pprint
import scipy.misc
import struct
import numpy as np
from time import gmtime, strftime
from six.moves import xrange
import os
import shutil

import tensorflow as tf
import tensorflow.contrib.slim as slim

pp = pprint.PrettyPrinter()

get_stddev = lambda x, k_h, k_w: 1/math.sqrt(k_w*k_h*x.get_shape()[-1])

def show_all_variables():
  model_vars = tf.trainable_variables()
  slim.model_analyzer.analyze_vars(model_vars, print_info=True)

def mkdir(path):
	if not os.path.exists(path): os.mkdir(path)

def mkdir_del(path):
    if os.path.exists(path): 
        shutil.rmtree(path)
        os.mkdir(path)
    else:        
        os.mkdir(path)
        
def read_idx(filename):
    ''' read idx files, such as: MNIST'''
    with open(filename, 'rb') as f:
        zero, data_type, dims = struct.unpack('>HBB', f.read(4))
        shape = tuple(struct.unpack('>I', f.read(4))[0] for d in range(dims))
        return np.fromstring(f.read(), dtype=np.uint8).reshape(shape)
