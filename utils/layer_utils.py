import numpy as np
import tensorflow as tf

def init_weights(name, shape, stddev):
	"""
	Handy helper function for initializing the weights of a layer.

	Performs He. et al. initilization as described in [1].

	References
	----------
	[1] - https://arxiv.org/abs/1502.01852
	"""
	weight = tf.Variable(tf.random_normal(shape, stddev=stddev),name="weight")
	return weight

def init_bias(name, shape, stddev, trans=False, bias_start_zero=False):
	"""
	Handy helper function for initializing the biases of a layer.

	Performs zero bias initialization.
	"""
	init = tf.zeros_initializer
	# bias = tf.Variable(tf.random_normal([shape[-1]], stddev=stddev),name="bias")
	bias = tf.Variable(tf.zeros([shape[-1]]), name="bias")
	# if bias_start_zero:
	if trans:
		x = np.array([[1., 0, 0], [0, 1., 0]])
		x = x.astype('float32').flatten()
		bias = tf.Variable(initial_value=x)

	return bias

def lrelu(x, leak=0.2, name="lrelu"):
    return tf.maximum(x, leak*x)

def binary_activation(x):

    cond = tf.less(x, 0.5*tf.ones(tf.shape(x)))
    out = tf.where(cond, tf.zeros(tf.shape(x)), tf.ones(tf.shape(x)))

    return out

def spiky(x):
    cond = tf.less_equal(x, tf.constant(0.5))
    return tf.where(cond,  tf.zeros(tf.shape(x)), x)

def Conv2D(input_tensor, input_shape, filter_size, num_filters, stddev, strides=1, name=None):
	"""
	Handy helper function for convnets.

	Performs 2D convolution with a default stride of 1. The kernel has shape
	filter_size x filter_size with num_filters output filters.
	"""
	shape = [filter_size, filter_size, input_shape, num_filters]

	# initialize weights and biases of the convolution
	W = init_weights(name=name+'_W', shape=shape, stddev=stddev)
	b = init_bias(name=name+'_b', shape=shape, stddev=stddev)

	conv = tf.nn.conv2d(input_tensor, W, strides=[1, strides, strides, 1], padding='SAME', name=name)
	conv = tf.nn.bias_add(conv, b)
	return conv

def Deconv2D(input_, output_shape, stddev, k_h=5, k_w=5, d_h=2, d_w=2, name="deconv2d", with_w=False):
    with tf.variable_scope(name):
        # filter : [height, width, output_channels, in_channels]
        w = tf.get_variable('w',
                            [k_h, k_w, output_shape[-1], input_.get_shape()[-1]],
                            initializer=tf.random_normal_initializer(stddev=stddev))
        try:
            deconv = tf.nn.conv2d_transpose(input_,
                                            w,
                                            output_shape=output_shape,
                                            strides=[1, d_h, d_w, 1])
        # Support for verisons of TensorFlow before 0.7.0
        except AttributeError:
            deconv = tf.nn.deconv2d(input_,
                                    w,
                                    output_shape=output_shape,
                                    strides=[1, d_h, d_w, 1])

        biases = tf.get_variable('biases',
                                [output_shape[-1]],
                                initializer=tf.constant_initializer(0.0))
        deconv = tf.reshape(tf.nn.bias_add(deconv, biases), deconv.get_shape())

        if with_w:
            return deconv, w, biases
        else:
            return deconv

def MaxPooling2D(input_tensor, k=2, use_relu=False, name=None):
	"""
	Handy wrapper function for convolutional networks.

	Performs 2D max pool with a default stride of 2.
	"""
	pool = tf.nn.max_pool(input_tensor, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding='SAME', name=name)

	if use_relu:
		pool = tf.nn.relu(pool)

	return pool

def BatchNormalization(input_tensor, phase=True, momentum=0.9, epsilon=1e-5, use_relu=False, name=None):
	"""
	Handy wrapper function for convolutional networks.

	Performs batch normalization on the input tensor.
	"""
	normed = tf.contrib.layers.batch_norm(input_tensor, decay=momentum,
										  updates_collections=None, epsilon=epsilon,
										  center=True, scale=True, is_training=phase, scope=name)

	if use_relu:
		normed = tf.nn.relu(normed)

	return normed

# class batch_norm(object):
#     def __init__(self, epsilon=1e-5, momentum = 0.9, name="batch_norm"):
#         with tf.variable_scope(name):
#             self.epsilon  = epsilon
#             self.momentum = momentum
#             self.name = name
#     def __call__(self, x, train=True):
#         return tf.contrib.layers.batch_norm(x,
#                     decay=self.momentum,
#                     updates_collections=None,
#                     epsilon=self.epsilon,
#                     scale=True,
#                     is_training=train,
#                     scope=self.name)

def Flatten(layer):
	"""
	Handy function for flattening the result of a conv2D or
	maxpool2D to be used for a fully-connected (affine) layer.
	"""
	layer_shape = layer.get_shape()
	# num_features = tf.reduce_prod(tf.shape(layer)[1:])
	num_features = layer_shape[1:].num_elements()
	layer_flat = tf.reshape(layer, [-1, num_features])

	return layer_flat, num_features

def Dense(input_tensor, num_inputs, num_outputs, stddev, use_relu=True, trans=False, name=None, with_w=False):
	"""
	Handy wrapper function for convolutional networks.

	Performs an affine layer (fully-connected) on the input tensor.
	"""
	shape = [num_inputs, num_outputs]

	# initialize weights and biases of the affine layer
	W = init_weights(name=name+'_W', shape=shape, stddev=stddev)
	b = init_bias(name=name+'_b', shape=shape, stddev=stddev, trans=trans)

	fc = tf.matmul(input_tensor, W, name=name) + b

	if use_relu:
		fc = tf.nn.relu(fc)
	if with_w:
		return fc, W, b
	else:
		return fc

def theta_bias(name):
	with tf.variable_scope(name):
		x = np.array([[1., 0, 0], [0, 1., 0]])
		x = x.astype('float32').flatten()
		return tf.Variable(initial_value=x)
