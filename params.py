from optparse import OptionParser
import scipy.linalg
import numpy as np
# fit a warp between two sets of points
def fit(Xsrc,Xdst,warpType):
    ptsN = len(Xsrc)
    X,Y,U,V,O,I = Xsrc[:,0],Xsrc[:,1],Xdst[:,0],Xdst[:,1],np.zeros([ptsN]),np.ones([ptsN])
    if warpType=="similarity":
        A = np.concatenate((np.stack([X,-Y,I,O],axis=1),
        					np.stack([Y,X,O,I],axis=1)),axis=0)
        b = np.concatenate((U,V),axis=0)
        p = scipy.linalg.lstsq(A,b)[0].squeeze()
        warpMtrx = np.array([[p[0],-p[1],p[2]],[p[1],p[0],p[3]],[0,0,1]],dtype=np.float32)
    elif warpType=="affine":
        A = np.concatenate((np.stack([X,Y,I,O,O,O],axis=1),
        					np.stack([O,O,O,X,Y,I],axis=1)),axis=0)
        b = np.concatenate((U,V),axis=0)
        p = scipy.linalg.lstsq(A,b)[0].squeeze()
        warpMtrx = np.array([[p[0],p[1],p[2]],[p[3],p[4],p[5]],[0,0,1]],dtype=np.float32)
    return warpMtrx

def set():
    parser = OptionParser()
    parser.add_option("--GPUID", dest="GPUID", metavar="GPUID", default="0", help="This variable holds the name of the dataset that you will like to align", type="string")
    parser.add_option("--grow", dest="grow", metavar="grow", default="0", help="This variable holds the name of the dataset that you will like to align", type="int")
    
    parser.add_option("--width"   , default="320", dest="width"   , metavar="100" , help="Image width", type="int")
    parser.add_option("--height"  , default="240", dest="height"  , metavar="100" , help="Image height", type="int")
    parser.add_option("--channels", default="3"  , dest="channels", metavar="25"  , help="Image number of channels", type="int")
    
    parser.add_option("--dataset", dest="dataset", metavar="test5", default="test5", help="This variable holds the name of the dataset that you will like to align", type="string")
    parser.add_option("--classNum", dest="classNum", metavar="classNum", default="0", help="This variable holds the name of the dataset that you will like to align", type="int")
    
    parser.add_option("--fold", dest="fold", metavar="fold", default="1", help="This variable holds the name of the dataset that you will like to align", type="string")
    parser.add_option("--selectSize", dest="selectSize", metavar="selectSize", default="100", help="This variable holds the name of the dataset that you will like to align", type="string")
    parser.add_option("--jhmdb21set", dest="jhmdb21set", metavar="jhmdb21set", default="2", help="This variable holds the name of the dataset that you will like to align", type="string")
    parser.add_option("--idSplit", dest="idSplit", metavar="idSplit", default="0", help="This variable holds the name of the dataset that you will like to align", type="string")
    parser.add_option("--checkpoint_dir", dest="checkpoint", metavar="checkpoints", default="checkpoints", help="Directory name to save the checkpoints [checkpoint]", type="string")
    parser.add_option("--state", dest="state", metavar="train", default="train", help="This variable decide what we will do with the model train or test", type="string")
    parser.add_option("--train", dest="train", metavar="train", default="train", help="This variable decide what we will do with the model train or test", type="string")
    parser.add_option("--test", dest="test", metavar="test", default="test", help="This variable decide what we will do with the model train or test", type="string")
    parser.add_option("--norm", dest="norm",metavar="1", default="1", help="This variable decide what we will do with the model train or test", type="int")
    
    parser.add_option("--batch_size", dest="batch_size", metavar="32", default="32", help="The size of batch images.", type="int")
    parser.add_option("--lr", dest="learning_rate", metavar="0.0002", default="0.0002", help="Learning rate for ADAM", type="float")
    parser.add_option("--beta1", dest="beta1", metavar="0.9", default="0.9", help="Momentum term of ADAM", type="float")
    parser.add_option("--epoch", dest="epoch", metavar="25", default="25", help="Numbers of epochs to train", type="int")
    
    parser.add_option("--z_dim", dest="z_dim", metavar="z_dim", default="100", help="The channels of image to use", type="int")
    parser.add_option("--gf_dim", dest="gf_dim", metavar="gf_dim", default="64", help="The channels of image to use", type="int")
    parser.add_option("--df_dim", dest="df_dim", metavar="df_dim", default="64", help="The channels of image to use", type="int")
    
    parser.add_option("--tbName", dest="tbName", metavar="tensorboard", default="default", help="Name for the tensorboard summary", type="string")
    parser.add_option("--model", dest="model", metavar="model", default="stn", help="What model I will use", type="string")
    parser.add_option("--model2", dest="model2",metavar="model2",default="None", help="What model I will use",type="string")
    parser.add_option("--typeData", dest="typeData",metavar="typeData",default="DIGEN",help="What model I will use",type="string")

    (options, args) = parser.parse_args()
    options.visBlockSize = 8
    options.pDim = 4
    return (options,args)
