#!/usr/bin/python3
from train.LSTM_DI import *
from train.t_inceptionResnetv2 import *

class TrainFactory:
    @staticmethod
    def instance(sess, options):
        train = options.get('experiment','train')
        if train == "t_incResnetV2":
            return TrainIncResnetV2(sess, options)
        elif train == "LSTM_DI":
            return TrainLSTM_DI(sess, options)
