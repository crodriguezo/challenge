#!/usr/bin/python3
import os
import cv2
import queue
import numpy as np

class DynamicGEN(object):

    def __init__(self, videoPath, label, shuffle=True):
        self.videoPath = videoPath
        self.label  = label

        self.framesPath = []
        self.nImages  = 0

        self.d = queue.Queue()
        self.d1 = queue.Queue()
        self.slD = queue.Queue()
        self.slI = queue.Queue()

        self.loadedFrames = 0

        self.readPaths()
        self.factors = self.fw(10)
        self.dynamicImages = np.arange(self.nImages)
        if shuffle == True:
            np.random.seed(4)
            np.random.shuffle(self.dynamicImages)

    def readPaths(self):
        frames = np.sort(os.listdir(self.videoPath))
        for frame in frames:
            if not frame.startswith('.'):
                self.framesPath.append(self.videoPath+"/"+frame)
        self.nImages = len(self.framesPath)-10

    def fw(self, length):
        fw = []
        for i in range(1,length+1):
            v = np.array(range(i,length+1))
            newV = (2*v-length-1)/v
            fw.append(np.sum(newV))
        return fw

    def normalizeDyn(self, di):
        mini = -15.4373015873
        maxi = 15.4373015873

        di = di/255
        return 2 * (di - mini)/(maxi-mini) - 1


    def normalizeRGB(self, im):
        return (im/127.5 - 1)

    def computeDI(self, it, less=-1):
        observedFrames = []
        f = 0
        img = None
        img_ret = None
        for i in range(it,it+10):
            if less-1 != f:
                img = cv2.imread(self.framesPath[i])
                observedFrames.append(img * self.factors[f])
            else:
                img_ret = cv2.imread(self.framesPath[i])
            f += 1
        observedFrames = np.array(observedFrames)
        DIo = np.zeros((240,320,3))

        DIo_R = np.sum(observedFrames[:,:,:,0],axis=0)
        DIo_G = np.sum(observedFrames[:,:,:,1],axis=0)
        DIo_B = np.sum(observedFrames[:,:,:,2],axis=0)

        DIo[:,:,0] = DIo_R
        DIo[:,:,1] = DIo_G
        DIo[:,:,2] = DIo_B

        if less != -1:
            return DIo, img_ret
        else:
            return DIo


    def load(self):
        d = self.computeDI(self.dynamicImages[self.loadedFrames])
        d1 = self.computeDI(self.dynamicImages[self.loadedFrames]+1)
        dSL, iSL = self.computeDI(self.dynamicImages[self.loadedFrames]+1, less=10)
        self.d.put(d)
        self.d1.put(d1)
        self.slD.put(dSL)
        self.slI.put(iSL)

        self.loadedFrames += 1
