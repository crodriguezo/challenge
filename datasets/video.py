#!/usr/bin/python3
import os
import cv2
import queue
import time
import numpy as np

from operator import itemgetter
from random import randint

class Video(object):

    def __init__(self, videoPath, annotations, stride, padding):
        self.videoName = videoPath.split('/')[-1]
        self.videoPath = videoPath
        self.pngPath   = videoPath.replace('.mp4','').replace('validation/','validation_png/')
        cap = cv2.VideoCapture(self.videoPath)
        if not cap.isOpened():
            print("could not open :",fn)
            return

        self.length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        self.width  = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.fps    = cap.get(cv2.CAP_PROP_FPS)

        self.trimsV = []
        for t in annotations:
            if int(float(t[0]) * self.fps) < self.length:
                init = int(float(t[0]) * self.fps)
                if int(float(t[1]) * self.fps) < self.length:
                    end = int(float(t[1]) * self.fps)
                else:
                    end = self.length
                self.trimsV.append((init, end, str(t[2])))
        self.trimsV = sorted(self.trimsV, key=itemgetter(0))
        np.random.seed(4)
        self.nSeq, self.mSeq = self.__genSequence(stride, randint(2,padding))

        self.factors = self.fw(10)
        self.loadedSequence = 0

    def __genSequence(self, stride, padding):
        self.sequences = []
        self.output = []
        # self.labels = []

        ## First sequence with the whole video frames.
        self.sequences.append((0,self.length))

        auxTrim = []
        for i in range(len(self.trimsV)):
            t0 = self.trimsV[i][0]
            tf = self.trimsV[i][1]
            auxTrim.append((t0, tf, self.trimsV[i][2]))
            # print(t0,tf,self.labelsV[i])
        self.output.append(auxTrim)

        ## Following sequences are the trims with some padding of random value from 2 to 10  frames per side.
        # auxSeq  = []
        # auxTrim = []
        # auxLab  = []
        # np.random.seed(4)
        # for i in range(len(self.trimsV)):
        #     t0 = self.trimsV[i][0]
        #     tf = self.trimsV[i][1]
        #
        #     init = t0 - padding if t0 - padding > 0 else t0
        #     end  = tf + padding if tf + padding < self.length else tf
        #
        #     self.sequences.append((init,end))
        #     self.output.append([(int(padding), tf - t0 + int(padding), self.trimsV[i][2])])
        #
        #
        # ## Next sequences are extracted using a sliding windows approach with stride 1 and 6 seconds of duration
        # auxSeq = []
        # for init in range(1, self.length, stride):
        #     end = init + int(randint(5,8) * self.fps)
        #     if end >= self.length:
        #         break
        #     else:
        #         auxSeq.append((init,end))
        #
        # for i in range(len(auxSeq)):
        #     s0 = auxSeq[i][0]
        #     sf = auxSeq[i][1]
        #
        #     flag = False
        #     auxTrim = []
        #     for j in range(len(self.trimsV)):
        #         t0 = self.trimsV[j][0]
        #         tf = self.trimsV[j][1]
        #
        #         if t0 > s0 and tf < sf:
        #             flag = True
        #             # print((s0,sf,t0,tf,t0-s0, tf-s0, self.trimsV[j][2]))
        #             auxTrim.append((int(t0-s0), int(tf-s0), self.trimsV[j][2]))
        #
        #     if flag == True:
        #         self.sequences.append(auxSeq[i])
        #         self.output.append(auxTrim)

        return len(self.sequences), len(self.output)

    def fw(self, length):
        fw = []
        for i in range(1,length+1):
            v = np.array(range(i,length+1))
            newV = (2*v-length-1)/v
            fw.append(np.sum(newV))
        return fw

    def normalizeDyn(self, di):
        mini = -15.4373015873
        maxi = 15.4373015873

        di = di/255
        return 2 * (di - mini)/(maxi-mini) - 1

    def load(self, inputs, net):
        start_time = time.time()
        #print(self.framesPath[self.loadedFrames])
        init = self.sequences[self.loadedSequence][0]
        end  = self.sequences[self.loadedSequence][1]
        DIs = []
        print(self.videoName, self.output[self.loadedSequence], self.fps, len(self.sequences), len(self.output), self.loadedSequence, init, end, round((end-init)/10), end=" ")
        for it in range(init+1, end+1):
            ending = it + 10
            if ending > end:
                ending = end + 1
            observedFrames = []
            factors = self.fw(ending-it)
            f=0
            for i in range(it,ending):
                img = cv2.imread("{}/{}.png".format(self.pngPath,str(i).zfill(9)) )
                observedFrames.append(img * factors[f])
                f += 1

            observedFrames = np.array(observedFrames)
            DIo = np.zeros((240,320,3))

            DIo_R = np.sum(observedFrames[:,:,:,0],axis=0)
            DIo_G = np.sum(observedFrames[:,:,:,1],axis=0)
            DIo_B = np.sum(observedFrames[:,:,:,2],axis=0)

            DIo[:,:,0] = DIo_R
            DIo[:,:,1] = DIo_G
            DIo[:,:,2] = DIo_B

            DIs.append(self.normalize(DIo))
        DIs   = np.array(DIs)

        aux_f = []
        for it in range(0,int(DIs.shape[0]/32)+1):
            init   = it * 32
            if init < DIs.shape[0]:
                ending = init + 32
                print(init,ending)
                feeding = {inputs: DIs[init:ending,:,:,:]}
                f = net.eval(feeding)
                aux_f.extend(f)

        features = np.array(aux_f)

        print("DIs shape: ",DIs.shape)
        print(self.output[self.loadedSequence][0][0])
        print(self.output[self.loadedSequence][0][1])
        print(self.output[self.loadedSequence][0][2])
        print("Features shape: ",features.shape)
        print("{}".format(time.time() - start_time))
        # img = cv2.imread(self.framesPath[self.loadedFrames])
        # #print(img.shape)
        # img = cv2.resize(img, (320,240))
        # self.images.put(img)
        ret = self.output[self.loadedSequence]
        self.loadedSequence += 1
        return features, ret, end-init

    def normalize(self, im):
        return (im/127.5 - 1.)
