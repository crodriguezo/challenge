#!/usr/bin/python3
import os
import sys
import csv
import numpy as np
import configparser

sys.path.append(os.getcwd())
from datasets.dataset import BaseDB
from datasets.video import Video

class ActivityNet(BaseDB):
    def __init__(self, options):
        self.options = options
        self.classes = []
        self.videos  = {'train':[], 'test':[]}
