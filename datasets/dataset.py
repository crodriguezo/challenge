#!/usr/bin/python3
import cv2
import numpy as np

class BaseDB(object):
    def loadDB(self):
        raise NotImplementedError

    def getBatch(self, model, init, end):
        raise NotImplementedError

    ## norm-
    def norm1(self, imInput):
        ret1 = imInput.copy()

        ret1[:,:,0] = (ret1[:,:,0]+abs(np.min(ret1[:,:,0])))/(np.max(ret1[:,:,0])+abs(np.min(ret1[:,:,0]))+1e-50)
        ret1[:,:,1] = (ret1[:,:,1]+abs(np.min(ret1[:,:,1])))/(np.max(ret1[:,:,1])+abs(np.min(ret1[:,:,1]))+1e-50)
        ret1[:,:,2] = (ret1[:,:,2]+abs(np.min(ret1[:,:,2])))/(np.max(ret1[:,:,2])+abs(np.min(ret1[:,:,2]))+1e-50)

        return ret1

    def norm2(self, imInput):
        ret1 = imInput.copy()

        ret1[:,:,0] = 2 * (ret1[:,:,0]+abs(np.min(ret1[:,:,0])))/(np.max(ret1[:,:,0])+abs(np.min(ret1[:,:,0]))+1e-50) - 1
        ret1[:,:,1] = 2 * (ret1[:,:,1]+abs(np.min(ret1[:,:,1])))/(np.max(ret1[:,:,1])+abs(np.min(ret1[:,:,1]))+1e-50) - 1
        ret1[:,:,2] = 2 * (ret1[:,:,2]+abs(np.min(ret1[:,:,2])))/(np.max(ret1[:,:,2])+abs(np.min(ret1[:,:,2]))+1e-50) - 1

        return ret1

    def norm3(self, imInput):
        ''' Input DIt and DIt+1
            substract the mean and divided by the std then use 2 * minmax normalization - 1 to have values between 0 and 1.'''
        ret1 = imInput.copy()
        ret1 = self.substractMean(ret1)
        ret1 = self.divideSTD(ret1)

        return self.norm2(ret1)

    def norm4(self, imInput):
        ret1 = imInput.copy()
        ret1 = self.substractMean(ret1)
        ret1 = self.divideSTD(ret1)
        return self.norm1(ret1)

    def norm5(self, imInput):
        ret1 = imInput.copy()
        ret1 = self.substractMean(ret1)
        ret1 = self.divideSTD(ret1)
        return ret1

    def substractMean(self, imInput):
        ret1 = imInput.copy()

        ret1[:,:,0] = (ret1[:,:,0] - self.Rmean)
        ret1[:,:,1] = (ret1[:,:,1] - self.Gmean)
        ret1[:,:,2] = (ret1[:,:,2] - self.Bmean)

        return ret1

    def divideSTD(self, imInput):
        ret1 = imInput.copy()

        ret1[:,:,0] = ret1[:,:,0] / self.Rstd
        ret1[:,:,1] = ret1[:,:,1] / self.Gstd
        ret1[:,:,2] = ret1[:,:,2] / self.Bstd

        return ret1
    
    def loadDB(self, state, all=False):
        if state == 'train':
            if all == True:
                it = 1
                for idx in self.trainIndex:
                    self.videos[state][idx].load()
                    print(self.videos[state][idx].videoPath)
                    print("Loading {}ing frames {}, {}/{}, {:.2f}%".format(state, self.loadingBatch, it, self.nFrameTr, it*100/self.nFrameTr),end='\r',flush=True)
                    it += 1

                self.loadingBatch += 1
            else:
                it = 1
                init = self.options.batch_size*100*self.loadingBatch
                end  = self.options.batch_size*100*(self.loadingBatch+1)
                for idx in (self.trainIndex[init:end]):
                    print(self.videos[state][idx].videoPath)
                    self.videos[state][idx].load()
                    print(self.videos[state][idx].videoPath)
                    print("Loading {}ing frames {}, {}/{}, {:.2f}%".format(state, self.loadingBatch, init+it, end, it*100/end),end='\r',flush=True)
                    it += 1

                self.loadingBatch += 1
        elif state== 'test':
            if all == True:
                it = 1
                for idx in self.testIndex:
                    self.videos[state][idx].load()
                    print(self.videos[state][idx].videoPath)
                    print("Loading {}ing frames {}, {}/{}, {:.2f}%".format(state, self.loadingBatch, it, self.nFrameTe, it*100/self.nFrameTe),end='\r',flush=True)
                    it += 1

                self.loadingBatch += 1
            else:
                it = 1
                init = self.options.batch_size*100*self.loadingBatch
                end  = self.options.batch_size*100*(self.loadingBatch+1)
                for idx in (self.testIndex[init:end]):
                    self.videos[state][idx].load()
                    print(self.videos[state][idx].videoPath)
                    print("Loading {}ing frames {}, {}/{}, {:.2f}%".format(state, self.loadingBatch, init+it, end, idx*100/end),end='\r',flush=True)
                    it += 1

                self.loadingBatch += 1

    def resetVideo(self, state):
        self.loadingBatch = 0
        if state == 'train':
            for video in self.videos['train']:
                video.loadedFrames = 0
        elif state == 'test':
            for video in self.videos['test']:
                video.loadedFrames = 0

    def getBatchRGB(self, state, init, end):
        frames = []
        labels = []
        if state == 'train':
            indexes = self.trainIndex[init:end]
            for index in indexes:
                video = self.videos['train'][index]
                if video.images.empty():
                    self.loadDB('train')
                frame = video.images.get()
                frames.append(video.normalize(frame))

                label = np.zeros(self.nClasses)
                label[np.where(np.array(self.classes) == video.label)[0][0]] = 1
                labels.append(label)

            frames = np.array(frames)
            labels = np.array(labels)
        elif state == 'test':
            indexes = self.testIndex[init:end]
            for index in indexes:
                video = self.videos['test'][index]
                if video.images.empty():
                    self.loadDB('test')
                frame = video.images.get()
                frames.append(video.normalize(frame))

                label = np.zeros(self.nClasses)
                label[np.where(np.array(self.classes) == video.label)[0][0]] = 1
                labels.append(label)

            frames = np.array(frames)
            labels = np.array(labels)

        return frames, labels

    def getBatchGEN(self, state, init,end):
        D   = []
        Dt1 = []
        SLD  = []
        SLI  = []
        labels = []
        if state == 'train':
            indexes = self.trainIndex[init:end]
            for index in indexes:
                video = self.videos['train'][index]
                if video.d.empty():
                    self.loadDB('train')
                d = video.d.get()
                d1 = video.d1.get()
                slD = video.slD.get()
                slI = video.slI.get()

                D.append(video.normalizeDyn(d))
                Dt1.append(video.normalizeDyn(d1))
                SLD.append(video.normalizeDyn(slD))
                SLI.append(video.normalizeRGB(slI))

                label = np.zeros(self.nClasses)
                label[np.where(np.array(self.classes) == video.label)[0][0]] = 1
                labels.append(label)

            D = np.array(D)
            Dt1 = np.array(Dt1)
            SLD = np.array(SLD)
            SLI = np.array(SLI)
            labels = np.array(labels)

        return D, Dt1, SLD, SLI, labels

    def augmentation(self, frames, labels, seed=-1):
        datagen = ImageDataGenerator(
                    rotation_range=20,
                    featurewise_center=False,
                    samplewise_center=False,
                    featurewise_std_normalization=False,
                    samplewise_std_normalization=False,
                    zca_whitening=False,
                    #contrast_stretching=True,
                    #adaptive_equalization=True,
                    #histogram_equalization=True,
                    width_shift_range=0.2,
                    height_shift_range=0.2,
                    shear_range=0.2,
                    zoom_range=0.2,
                    horizontal_flip=True,
                    channel_shift_range=0.3,
                    fill_mode='nearest')
        if seed == -1:
            return datagen.flow(frames, labels, batch_size=self.options.batch_size, shuffle=False)
        else:
            return datagen.flow(frames, labels, batch_size=self.options.batch_size, shuffle=False, seed=seed)
