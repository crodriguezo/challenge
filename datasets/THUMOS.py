#!/usr/bin/python3
import os
import sys
import csv
import numpy as np
import configparser
import tensorflow as tf

sys.path.append(os.getcwd())
from datasets.dataset import BaseDB
from datasets.video import Video

class THUMOS(BaseDB):
    def __init__(self, options, readDir=True):
        self.options = options
        self.classes = []
        self.videos  = {'train':{}, 'test':{}}
        self.features = {'train': [], 'validation': []}
        self.loadingBatch = 0
        if readDir == True:
            self.readDirectories()

    def __readANNO(self, directory, files, set='train'):
        videoNames = {}
        for f in files:
            if not str(f).startswith('Ambiguous_'):
                c_ = str(f).split('_')[0]
                self.classes.append(str(c_))
                annoFile = open(directory+f)
                for anno in annoFile:
                    # print(str(c_))
                    videoName, _g, begin, end = anno.replace('\n','').split(' ')
                    if videoName in videoNames:
                        videoNames[videoName].append((begin,end,c_))
                    else:
                        videoNames[videoName] = []
                        videoNames[videoName].append((begin,end,c_))

        aux = 0
        for key in videoNames:
            auxV = Video(self.options.get('thumos',set+'_videos')+key+'.mp4', videoNames[key], int(self.options.get('thumos','stride')), int(self.options.get('thumos','padding')))
            self.videos[set][key] = auxV
            aux += auxV.nSeq
            arrIndex = [key for i in range(auxV.nSeq)]
            # print(auxV.length, auxV.nSeq, len(arrIndex))#, auxV.videoPath)
            # self.sequences[set].append(arrIndex)

    def readDirectories(self):
        train_anno = self.options.get('thumos','train_anno')
        test_anno  = self.options.get('thumos','test_anno')

        self.classes = []
        self.trainIndex = []
        self.testIndex  = []

        file_train = np.sort(os.listdir(train_anno))
        file_test  = np.sort(os.listdir(test_anno))

        self.__readANNO(train_anno, file_train, 'train')
        self.__readANNO(test_anno, file_test, 'test')

        self.classes  = np.unique(self.classes)
        self.nClasses = len(self.classes)

        self.nVidTr = 0
        self.nVidTe = 0

        self.nVidTr = len(self.videos['train'])
        self.nVidTe = len(self.videos['test'])

        print('# of videos for training {}'.format(self.nVidTr))
        print('# of videos for testing  {}'.format(self.nVidTe))
        print("# classes: {}".format(self.nClasses))

        numSeq = 0
        for key in self.videos['train']:
            numSeq = self.videos['train'][key].nSeq
            for it in range(numSeq):
                self.trainIndex.append(key)

        for key in self.videos['test']:
            numSeq = self.videos['test'][key].nSeq
            for it in range(numSeq):
                self.testIndex.append(key)

        self.trainIndex = sorted(np.array(self.trainIndex))
        self.testIndex  = sorted(np.array(self.testIndex))
        #np.random.seed(4)
        #np.random.shuffle(self.trainIndex)
        #np.random.shuffle(self.testIndex)True

        self.nSeqTr = len(self.trainIndex)
        self.nSeqTe = len(self.testIndex)
        print("# of training sequences :", self.nSeqTr)
        print("# of testing sequences :", self.nSeqTe)

        lengths = []
        for key in self.videos['train']:
        #     if self.videos['train'][key].length == 265:
        #         print(self.videos['train'][key].labels[0], key)
        #         for i in range(len(self.videos['train'][key].labels)):
        #             print(self.videos['train'][key].labels[i])
        #True
            # lengths.append(self.videos['train'][key].length)
            for i in range(0,len(self.videos['train'][key].sequences)):
                # print(len(self.videos['train'][key].sequences))
                # print(self.videos['train'][key].sequences[i][1]-self.videos['train'][key].sequences[i][0])
                aux_length = (self.videos['train'][key].sequences[i][1]-self.videos['train'][key].sequences[i][0])
                if aux_length > 3500:
                    print(key, self.videos['train'][key].trimsV, self.videos['train'][key].length)
                lengths.append(aux_length)#/self.videos['train'][key].length)
            # for i in range(1,len(self.videos['train'][key].trimsV)):
                # if self.videos['train'][key].trimsV[i-1][2] == self.videos['train'][key].trimsV[i][2]:
                #     auxL = round(self.videos['train'][key].trimsV[i][1]-self.videos['train'][key].trimsV[i][0],2)
                #     lengths.append(auxL)
                # if auxL == 0.2:
                #     print(auxL, key, self.videos['train'][key].length)
                #     for i in range(len(self.videos['train'][key].labels)):
                #         print(self.videos['train'][key].labels[i],round(self.videos['train'][key].trims[i][1]-self.videos['train'][key].trims[i][0],2),self.videos['train'][key].trims[i][1]*self.videos['train'][key].fps,self.videos['train'][key].trims[i][0]*self.videos['train'][key].fps)

        lengths = np.array(lengths)
        mini = np.min(lengths)
        maxi = np.max(lengths)
        mean = np.mean(lengths)
        print("\nMin(frames) :{} \nMin(DI) :{}".format(mini, int(round(mini/10))) )
        print("Max(frames) :{} \nMax(DI) :{}".format(maxi, int(round(maxi/10))) )
        print("Mean(frames) :{:.2f} \nMean(DI) :{}".format(mean, int(round(mean/10))) )
        print(np.std(lengths))
        # print(lengths[lengths/10 > 500]/10)
        print("# Sequences with DI > 500 :{}".format(len(lengths[lengths/10 > 500])) )

    def loadNET(self, saver, checkpoint):
        import re
        import os
        print(" [*] Reading checkpoints...")
        checkpoint_dir = checkpoint
        print(checkpoint_dir)
        ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
        if ckpt and ckpt.model_checkpoint_path:
            ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
            saver.restore(self.sess, os.path.join(checkpoint_dir, ckpt_name))
            counter = int(next(re.finditer("(\d+)(?!.*\d)",ckpt_name)).group(0))
            print(" [*] Success to read {}".format(ckpt_name))
            return True, counter
        else:
            print(" [*] Failed to find a checkpoint")
            return False, 0

    def getFeatures(self, input, net, set, all=False):
        if all == True:
            if set == 'train':
                indexes = self.trainIndex
            else:
                indexes = self.testIndex
            features = []
            labels   = []
            lengths  = []
            old_index = indexes[0]
            for i in indexes[104:]:
                print(set,i)
                if old_index != i:
                    print(len(features))
                    features = np.array(features)
                    labels   = np.array(labels)
                    np.save("/home/crodriguezo/testing_features/features_{}".format(old_index),features)
                    np.save("/home/crodriguezo/testing_features/labels_{}".format(old_index),labels)
                    np.save("/home/crodriguezo/testing_features/lengths_{}".format(old_index),lengths)
                    features = []
                    labels   = []
                    lengths  = []

                sequence, output, length = self.videos[set][i].load(input, net)
                features.append(sequence)
                labels.append(output)
                lengths.append(length)
                old_index = i

            features = np.array(features)
            labels   = np.array(labels)
            lengths  = np.array(lengths)
            np.save("/home/crodriguezo/testing_features/features_{}".format(old_index),features)
            np.save("/home/crodriguezo/testing_features/labels_{}".format(old_index),labels)
            np.save("/home/crodriguezo/testing_features/lengths_{}".format(old_index),lengths)
            features = []
            labels   = []
            lengths  = []

    def loadDB(self):
        keys = list(self.videos['train'].keys())
        keys = np.array(keys)
        np.random.seed(5)
        np.random.shuffle(keys)
        tsize = int(len(keys)*0.8)
        print("Training {}, Testing {}".format(tsize, len(keys) - tsize))
        training = keys[:tsize]
        validation = keys[tsize:]
        tr_features = []
        tr_labels   = []
        tr_lengths  = []
        i = 0
        for key in training:
            i+=1
            print("Loading features and labels from training videos {}% - video {}".format(int(i*100/len(training)), key), end='\r' )
            fe = np.load(self.options.get('thumos','features')+"features_{}.npy".format(key))
            la = np.load(self.options.get('thumos','labels')+"labels_{}.npy".format(key))
            le = np.load(self.options.get('thumos','lengths')+"lengths_{}.npy".format(key))
            tr_features.extend(fe)
            tr_labels.extend(la)
            tr_lengths.extend(le)
        print("")
        print(len(tr_features), len(tr_labels))

        tr_indexes = range(len(tr_labels))
        tr_indexes = np.array(tr_indexes)
        # np.random.shuffle(tr_indexes)
        #print(tr_indexes)
        self.tr_frames   = []
        self.tr_length   = []
        self.tr_features = []
        self.tr_labels   = []
        for indx in tr_indexes:
            seqN = tr_features[indx].shape[0]
            self.tr_length.append(seqN)
            self.tr_frames.append(tr_lengths[indx])
            self.tr_features.append(tr_features[indx])
            self.tr_labels.append(tr_labels[indx])


        val_features = []
        val_labels   = []
        val_lengths  = []
        i = 0
        for key in validation:
            i+=1
            print("Loading features and labels from validation videos {}% - video {}".format(int(i*100/len(validation)), key), end='\r' )
            f = np.load(self.options.get('thumos','features')+"features_{}.npy".format(key))
            l = np.load(self.options.get('thumos','labels')+"labels_{}.npy".format(key))
            le = np.load(self.options.get('thumos','lengths')+"lengths_{}.npy".format(key))
            val_features.extend(f)
            val_labels.extend(l)
            val_lengths.extend(le)
        print("")
        print(len(val_features), len(val_labels))

        val_indexes = range(len(val_labels))
        val_indexes = np.array(val_indexes)
        #np.random.shuffle(val_indexes)
        self.val_length   = []
        self.val_features = []
        self.val_labels   = []
        self.val_frames   = []
        for indx in val_indexes:
            seqN = val_features[indx].shape[0]
            self.val_length.append(seqN)
            self.val_frames.append(val_lengths[indx])
            self.val_features.append(val_features[indx])
            self.val_labels.append(val_labels[indx])

        self.tr_length    = np.array(self.tr_length)+1
        self.tr_features  = np.array(self.tr_features)
        self.tr_labels    = np.array(self.tr_labels)

        self.val_length   = np.array(self.val_length)+1
        self.val_features = np.array(self.val_features)
        self.val_labels   = np.array(self.val_labels)
        self.val_frames   = np.array(self.val_frames)

        print(np.max(self.tr_length), np.mean(self.tr_length), np.min(self.tr_length))
        return len(tr_indexes), len(val_indexes)

    def __IoU(self, gt, proposal, length):
        gts = []
        IoU = 0
        for i in range(len(gt)):
            med_frame = proposal[0]*length
            init_proposal = med_frame - proposal[1]*length
            end_proposal  = med_frame + proposal[1]*length
            if int(gt[i][1]) <= init_proposal:
                intersection = 0.0
            elif int(gt[i][0]) >= end_proposal:
                intersection = 0.0
            else:
                inter_i = int(gt[i][0]) if int(gt[i][0]) >= init_proposal else init_proposal
                inter_e = int(gt[i][1]) if int(gt[i][1]) <= end_proposal else end_proposal
                intersection = inter_e - inter_i
            union = (int(gt[i][1])-int(gt[i][0])) + (end_proposal - init_proposal) - intersection
            IoU = intersection/union
            # print(IoU, init_proposal, end_proposal, proposal)
            conf = 0
            if  IoU > 0.5:
                conf = 1.0
                return conf, IoU

        return 0.0, IoU


    def createProposal(self, di_init, amount, gt, length):
        conf = []
        med  = []
        std  = []
        # di_init += 1
        med_a  = np.array([0.125, 0.25, 0.5, 0.75, 0.875])*20 + np.ones(5) * di_init * 10 #np.random.uniform(0,1,amount)
        med_a  = med_a/length
        std_a  = [10,30,50,100]/length#np.random.uniform(0,0.5,4)
        for i in range(5):
            for j in range(4):
                confidence, IoU = self.__IoU(gt, (med_a[i], std_a[j]), length)
                conf.append(confidence)
                med.append(med_a[i])
                std.append(std_a[j])
        return conf, med, std

    def createProposal1(self, di_init, mean, std, amount, gt, fMax):
        conf = []
        beg  = []
        end  = []
        r_ending = np.random.uniform(mean,std,amount)
        for i in range(1, amount+1):
            init  = di_init * amount + i
            final = int(init + np.abs(r_ending[i-1]))
            final = final if final < fMax else fMax
            confidence, IoU = self.__IoU(gt, (init, final))
            conf.append(confidence)
            beg.append(init)
            end.append(final)
            # print("{} {:.2f} {} {} {}".format(confidence, IoU, init, final, r_ending[i-1]))
        return conf, beg, end

    def createProposal2(self, amount, gt):
        conf = []
        beg  = []
        end  = []
        r_ending = np.random.uniform(mean,std,amount)
        for i in range(1, amount+1):
            final = di_init * amount + i
            init  = int(final - np.abs(r_ending[i-1]))
            init =  init if init > 0 else 1
            confidence, IoU = self.__IoU(gt, (init, final))
            conf.append(confidence)
            beg.append(init)
            end.append(final)
            # print("{} {:.2f} {} {} {}".format(confidence, IoU, init, final, r_ending[i-1]))
        return conf, beg, end

    def getBatch(self, set, init, end):

        if set == 'train':
            input, length = self.tr_features[init:end], self.tr_length[init:end]
            output, frames = self.tr_labels[init:end], self.tr_frames[init:end]

            ## Obtaining the max sequence to feed arrays if the same size using padding
            maxSeq = 0
            for indx in range(input.shape[0]):
                seqN = input[indx].shape[0]
                if seqN > maxSeq:
                    maxSeq = seqN
            maxSeq=maxSeq+1

            ## Generating proposals
            proposals = []
            for indx in range(input.shape[0]):
                seqN = input[indx].shape[0]
                seq_propo = []
                for o in range(seqN):
                    proposal = []
                    # aux = []
                    conf, med, std = self.createProposal(o, 20, output[indx], frames[indx])
                    proposal = conf
                    proposal.extend(med)
                    proposal.extend(std)
                    seq_propo.append(proposal)
                seq_propo = np.array(seq_propo)
                seq_propo = np.concatenate((seq_propo, np.zeros((maxSeq-seqN, 15 * 4))), axis=0)

                seq_propo = np.array(seq_propo)
                proposals.append(seq_propo)
            proposals = np.array(proposals)

            newInput = []
            for indx in range(input.shape[0]):
                seqN = input[indx].shape[0]
                aux = input[indx]
                aux = np.concatenate((aux, np.zeros((maxSeq-seqN,1536))), axis=0)
                newInput.append(aux)
            input = np.array(newInput)

        elif set == 'vali':
            input, length = self.val_features[init:end], self.val_length[init:end]
            output, frames = self.val_labels[init:end], self.val_frames[init:end]

            ## Obtaining the max sequence to feed arrays if the same size using padding
            maxSeq = 0
            for indx in range(input.shape[0]):
                seqN = input[indx].shape[0]
                if seqN > maxSeq:
                    maxSeq = seqN
            maxSeq=maxSeq+1

            ## Generating proposals
            proposals = output
            # for indx in range(input.shape[0]):
            #     seqN = input[indx].shape[0]
            #     seq_propo = []
            #     for o in range(seqN):
            #         proposal = []
            #         conf, med, std = self.createProposal(o, 20, output[indx], frames[indx])
            #         proposal = conf
            #         proposal.extend(med)
            #         proposal.extend(std)
            #         seq_propo.append(proposal)
            #     seq_propo = np.array(seq_propo)
            #     seq_propo = np.concatenate((seq_propo, np.zeros((maxSeq-seqN, 15 * 4))), axis=0)
            #
            #     seq_propo = np.array(seq_propo)
            #     proposals.append(seq_propo)
            # proposals = np.array(proposals)

            newInput = []
            for indx in range(input.shape[0]):
                seqN = input[indx].shape[0]
                aux = input[indx]
                aux = np.concatenate((aux, np.zeros((maxSeq-seqN,1536))), axis=0)
                newInput.append(aux)
            input = np.array(newInput)

        return input, proposals, length, frames
