#!/usr/bin/python3
import sys
sys.path.append('../../activity-detection/src/')

import os
import numpy as np
import multiprocessing
import tensorflow as tf

from datasets.dataset import BaseDB
from datasets.video import Video
from datasets.dynamics import Dynamic
from datasets.dynamicsgen import DynamicGEN
# from datasets.optical import Optical

from keras.preprocessing.image import ImageDataGenerator
import multiprocessing
import utils.augmentation.image as T


class JHMDB(BaseDB):
    def __init__(self, options):
        self.options = options
        self.pathTo  = '/mnt/ssd/crodriguezo/JHMDB/'

        self.classes = []
        self.videos  = {'train':[], 'test':[]}

        self.readDirectories()
        self.loadingBatch = 0

    def readDirectories(self):
        '''

        '''
        pathFrames  = self.pathTo + "Rename_Images/"
        pathOptical = self.pathTo + "OF/"
        pathSplits  = self.pathTo + "splits/"
        print("Read directories to memory 0%",end='\r',flush=True)

        splits = np.sort(os.listdir(pathSplits))

        self.trainIndex = []
        self.testIndex  = []

        nVidTrAux = 0
        nVidTeAux = 0
        for split in splits:
            if '_split{}.txt'.format(self.options.jhmdb21set) in split:
                c_ = split.split("_test_")[0]
                self.classes.append(c_)

                lines = open(pathSplits+split)
                for line in lines:
                    video, set_ = line.replace("\n","").split(" ")
                    video = video.replace(".avi","")
                    set_  = int(set_)
                    if self.options.typeData == 'RGB':
                        imageAux = Video(pathFrames+c_+"/"+video, c_)
                    elif self.options.typeData == 'DI':
                        imageAux = Dynamic(pathFrames+c_+"/"+video, c_)
                    elif self.options.typeData == 'DIGEN':
                        imageAux = DynamicGEN(pathFrames+c_+"/"+video, c_)
                    elif self.options.typeData == 'OF':
                        imageAux = Video(pathOptical+c_+"/"+video, c_)
                    elif self.options.typeData == 'DOF':
                        imageAux = Dynamic(pathOptical+c_+"/"+video, c_)
                    if set_ == 1:
                        self.videos['train'].append(imageAux)
                        for it in range(imageAux.nImages):
                            self.trainIndex.append(nVidTrAux)
                        nVidTrAux += 1
                    elif set_ == 2:
                        self.videos['test'].append(imageAux)
                        for it in range(imageAux.nImages):
                            self.testIndex.append(nVidTeAux)
                        nVidTeAux += 1
                    print("Read directories to memory {:.2f}%".format(((nVidTrAux+nVidTeAux)/928)*100),flush=True,end='\r')
                # break

        self.nVidTr   = len(self.videos['train'])
        self.nVidTe   = len(self.videos['test'])
        self.nVideos  = self.nVidTr + self.nVidTe

        self.nClasses = len(self.classes)

        self.nFrameTr = 0
        self.nFrameTe = 0

        for video in self.videos['train']:
            self.nFrameTr += video.nImages

        for video in self.videos['test']:
            self.nFrameTe += video.nImages

        self.nFrames  = self.nFrameTr + self.nFrameTe
        print("\n# videos: {}\n# videos train: {}\n# videos test: {}\n".format(self.nVideos, self.nVidTr, self.nVidTe))
        print("# frames: {}\n# frames train: {}\n# frames test: {}\n".format(self.nFrames, self.nFrameTr, self.nFrameTe))
        print("# classes: {}".format(self.nClasses))

        self.trainIndex = np.array(self.trainIndex)
        self.testIndex  = np.array(self.testIndex)
        np.random.seed(4)
        np.random.shuffle(self.trainIndex)
        np.random.shuffle(self.testIndex)

    def loadDB(self, state, all=False):
        if state == 'train':
            if all == True:
                it = 1
                for idx in self.trainIndex:
                    self.videos[state][idx].load()
                    print(self.videos[state][idx].videoPath)
                    print("Loading {}ing frames {}, {}/{}, {:.2f}%".format(state, self.loadingBatch, it, self.nFrameTr, it*100/self.nFrameTr),end='\r',flush=True)
                    it += 1

                self.loadingBatch += 1
            else:
                it = 1
                init = self.options.batch_size*100*self.loadingBatch
                end  = self.options.batch_size*100*(self.loadingBatch+1)
                for idx in (self.trainIndex[init:end]):
                    self.videos[state][idx].load()
                    print(self.videos[state][idx].videoPath)
                    print("Loading {}ing frames {}, {}/{}, {:.2f}%".format(state, self.loadingBatch, init+it, end, it*100/end),end='\r',flush=True)
                    it += 1

                self.loadingBatch += 1
        elif state== 'test':
            if all == True:
                it = 1
                for idx in self.testIndex:
                    self.videos[state][idx].load()
                    print(self.videos[state][idx].videoPath)
                    print("Loading {}ing frames {}, {}/{}, {:.2f}%".format(state, self.loadingBatch, it, self.nFrameTe, it*100/self.nFrameTe),end='\r',flush=True)
                    it += 1

                self.loadingBatch += 1
            else:
                it = 1
                init = self.options.batch_size*100*self.loadingBatch
                end  = self.options.batch_size*100*(self.loadingBatch+1)
                for idx in (self.testIndex[init:end]):
                    self.videos[state][idx].load()
                    print(self.videos[state][idx].videoPath)
                    print("Loading {}ing frames {}, {}/{}, {:.2f}%".format(state, self.loadingBatch, init+it, end, idx*100/end),end='\r',flush=True)
                    it += 1

                self.loadingBatch += 1

    def resetVideo(self, state):
        self.loadingBatch = 0
        if state == 'train':
            for video in self.videos['train']:
                video.loadedFrames = 0
        elif state == 'test':
            for video in self.videos['test']:
                video.loadedFrames = 0

    def getBatchRGB(self, state, init, end):
        frames = []
        labels = []
        if state == 'train':
            indexes = self.trainIndex[init:end]
            for index in indexes:
                video = self.videos['train'][index]
                if video.images.empty():
                    self.loadDB('train')
                frame = video.images.get()
                frames.append(video.normalize(frame))

                label = np.zeros(self.nClasses)
                label[np.where(np.array(self.classes) == video.label)[0][0]] = 1
                labels.append(label)

            frames = np.array(frames)
            labels = np.array(labels)
        elif state == 'test':
            indexes = self.testIndex[init:end]
            for index in indexes:
                video = self.videos['test'][index]
                if video.images.empty():
                    self.loadDB('test')
                frame = video.images.get()
                frames.append(video.normalize(frame))

                label = np.zeros(self.nClasses)
                label[np.where(np.array(self.classes) == video.label)[0][0]] = 1
                labels.append(label)

            frames = np.array(frames)
            labels = np.array(labels)

        return frames, labels

    def getBatchGEN(self, state, init,end):
        D   = []
        Dt1 = []
        SLD  = []
        SLI  = []
        labels = []
        if state == 'train':
            indexes = self.trainIndex[init:end]
            for index in indexes:
                video = self.videos['train'][index]
                if video.d.empty():
                    self.loadDB('train')
                d = video.d.get()
                d1 = video.d1.get()
                slD = video.slD.get()
                slI = video.slI.get()

                D.append(video.normalizeDyn(d))
                Dt1.append(video.normalizeDyn(d1))
                SLD.append(video.normalizeDyn(slD))
                SLI.append(video.normalizeRGB(slI))

                label = np.zeros(self.nClasses)
                label[np.where(np.array(self.classes) == video.label)[0][0]] = 1
                labels.append(label)

            D = np.array(D)
            Dt1 = np.array(Dt1)
            SLD = np.array(SLD)
            SLI = np.array(SLI)
            labels = np.array(labels)

        return D, Dt1, SLD, SLI, labels

    def augmentation(self, frames, labels, seed=-1):
        datagen = ImageDataGenerator(
                    rotation_range=20,
                    featurewise_center=False,
                    samplewise_center=False,
                    featurewise_std_normalization=False,
                    samplewise_std_normalization=False,
                    zca_whitening=False,
                    #contrast_stretching=True,
                    #adaptive_equalization=True,
                    #histogram_equalization=True,
                    width_shift_range=0.2,
                    height_shift_range=0.2,
                    shear_range=0.2,
                    zoom_range=0.2,
                    horizontal_flip=True,
                    channel_shift_range=0.3,
                    fill_mode='nearest')
        if seed == -1:
            return datagen.flow(frames, labels, batch_size=self.options.batch_size, shuffle=False)
        else:
            return datagen.flow(frames, labels, batch_size=self.options.batch_size, shuffle=False, seed=seed)

if __name__ == '__main__':
    '''FOR USE MAIN THE RANGE OF IMAGES HAS TO BE BETWEEN 0 AND 1 NOT -1 AND 1 '''
    import time
    import params
    from matplotlib.backends.backend_pdf import PdfPages
    import matplotlib.pyplot as plt
    (options, args) = params.set()
    data = JHMDB(options)

    def plot_images_1(pdf, imgs, title):
        fig, ax = plt.subplots(6, 6, figsize=(10, 10))
        plt.suptitle(title, size=32)
        plt.setp(ax, xticks=[], yticks=[])
        plt.tight_layout(rect=[0, 0.03, 1, 0.95])
        for i in range(6):
            for j in range(6):
                if i*6 + j < 32:
                    ax[i][j].imshow(imgs[i*6 + j])
        pdf.savefig()

    batch_idxs = data.nFrameTr // options.batch_size

    for epoch in range(options.epoch):
        data.resetVideo('train')
        data.loadDB('train')
        for idx in range(batch_idxs):
            init = int(options.batch_size*idx)
            end  = int(options.batch_size*(idx+1))
            d, dt1, slD, slI, labels = data.getBatchGEN("train",init,end)
            print(np.max(d),np.min(d))
            with PdfPages('original_d.pdf') as pdf:
                plot_images_1(pdf,d,"original")
            with PdfPages('original_dt1.pdf') as pdf:
                plot_images_1(pdf,dt1,"original")
            with PdfPages('original_slD.pdf') as pdf:
                plot_images_1(pdf,slD,"original")
            with PdfPages('original_slI.pdf') as pdf:
                plot_images_1(pdf,slI,"original")
            i = 0
            for X, Y in data.augmentation(d, labels):
                startTime = time.time()
                with PdfPages("augmentation_{}".format(i)) as pdf:
                    plot_images_1(pdf,X,"augmentation_{}".format(i))
                endTime = time.time()
                i+=1
                if i > 7:
                    break
                print("Epoch {} | [{}/{}], {}, {}-{}".format(epoch, idx, batch_idxs, endTime - startTime, init, end))
            break
