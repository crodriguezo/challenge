#!/usr/bin/python3

from models.m_inceptionResnetv2 import *
# from models.cstn2 import *
# from models.icstn import *

class ModelFactory:

    @staticmethod
    def instance(model, config):
        if model == "InceptionResnetV2":
            return ModelInceptionResnetV2(config)
        # elif model == "LSTM_DI":
