import os
import time
import numpy as np
import tensorflow as tf
from train.train import *
from utils.utils import show_all_variables, mkdir_del

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

class TrainIncResnetV2(BaseTrain):
    def __init__(self, sess, model, options):
        self.sess = sess
        self.model = model
        self.options = options

        self.inputs  = tf.placeholder(tf.float32, [self.options.batch_size, self.options.height, self.options.width, self.options.channels], name='input')
        self.labels  = tf.placeholder(tf.float32, [self.options.batch_size, self.options.classNum], name ='labels')
        inputs = self.inputs
        labels = self.labels
        self.logits, _ = self.model.build(inputs)

        self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.logits, labels=labels))
        correct_prediction = tf.equal(tf.argmax(self.logits, 1), tf.argmax(labels, 1))
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        #self.saver = tf.train.Saver()

    def run(self, data):
        mkdir_del("../{}/models_{}".format(self.options.checkpoint,self.options.model))
        mkdir_del("../tensorboards/summary_{}/{}".format(self.options.train, self.options.tbName))
        batch_idxsTrain = data.nFrameTr // self.options.batch_size
        batch_idxsTest = data.nFrameTe // self.options.batch_size
        t_vars = tf.trainable_variables()

        lastLayer = []
        for var in t_vars:
            if self.model.checkpoint_exclude_scopes[0] in var.name:
                lastLayer.append(var)
            elif self.model.checkpoint_exclude_scopes[1] in var.name:
                lastLayer.append(var)

        global_step = tf.Variable(0, trainable=False)
        boundaries = [3930, 6550]
        values = [self.options.learning_rate, self.options.learning_rate/10, self.options.learning_rate/100]
        learning_rate = tf.train.piecewise_constant(global_step, boundaries, values)

        optim = tf.train.AdamOptimizer(self.options.learning_rate, beta1= self.options.beta1, epsilon=0.1).minimize(self.loss, global_step=global_step)
        #optim = tf.train.GradientDescentOptimizer(self.options.learning_rate).minimize(self.loss)
        ### Load inception score ##

        tf.global_variables_initializer().run()
        loader = tf.train.Saver(var_list=self.model.variablesToLoad())
        loader.restore(self.sess, '../data/weights/inception_resnet_v2_2016_08_30.ckpt')

        ### variables I should change it to metrics ##

        lossSummary     = tf.summary.scalar("training loss", self.loss)

        trainAccuracy   = tf.placeholder(tf.float32,shape=[])
        trainSummary    = tf.summary.scalar("train accuracy", trainAccuracy)

        valiAccuracy    = tf.placeholder(tf.float32,shape=[])
        valiSummary     = tf.summary.scalar("validation loss", valiAccuracy)

        tfSummaryWriter = tf.summary.FileWriter("../tensorboards/summary_{}/{}".format(self.options.train, self.options.tbName))


        tfSummaryWriter.add_graph(self.sess.graph)

        sumTimer = 0
        summaryCounter = 0
        self.saver = tf.train.Saver(tf.all_variables(), max_to_keep=self.options.epoch)
        auxl = 0
        meansTest = []
        meansTrain = []
        losses = []
        tAccuracy = []
        for epoch in range(self.options.epoch):
            data.resetVideo('train')
            data.loadDB('train')
            iteration = 1
            for idx in range(0, batch_idxsTrain):
                timeStart_iter = time.time()
                init = int(self.options.batch_size*idx)
                end  = int(self.options.batch_size*(idx+1))

                imgs, labels = data.getBatchRGB("train",init,end)
                feeding = {self.inputs: imgs, self.labels: labels}
                _, loss, summary = self.sess.run([optim, self.loss, lossSummary], feed_dict=feeding)
                losses.append(loss)
                print("Epoch: [{}] [{}/{}] time: {} loss: {} ".format(epoch, iteration, batch_idxsTrain*8, time.time() - timeStart_iter, loss))
                iteration +=1
                i = 1
                for X, Y in data.augmentation(imgs, labels):
                    timeStart_augmented = time.time()
                    feeding = {self.inputs: X, self.labels: Y}
                    _, loss, summary = self.sess.run([optim, self.loss, lossSummary], feed_dict=feeding)
                    print("Epoch: [{}] [{}/{}] time: {} loss: {} ".format(epoch, iteration, batch_idxsTrain*8, time.time() - timeStart_augmented, loss))
                    iteration +=1
                    i+=1
                    if i > 7:
                        break

                if (idx+1) % 2 == 0:
                    summaryCounter += 1
                    tfSummaryWriter.add_summary(summary,summaryCounter)

                if (idx+1) % 1 == 0:
                    train_accuracy = self.accuracy.eval(feed_dict=feeding)
                    tAccuracy.append(train_accuracy)
                    #print("Train Accuracy : {}".format(train_accuracy))
                timeEnd_iter = time.time()
            savePath = self.saver.save(self.sess,"../checkpoints/models_{}{}/{}_epoch_{}_{}.ckpt".format(self.options.model,self.options.tbName,self.options.dataset,epoch,auxl))

            with PdfPages('./mail/loss.pdf') as pdf:
                plt.plot(range(len(losses)), losses, '-')
                plt.title('Loss - {} set {}'.format(self.options.dataset, self.options.jhmdb21set))
                pdf.savefig()  # saves the current figure into a pdf page
                plt.close()

            with PdfPages('./mail/training_accu.pdf') as pdf:
                plt.plot(range(len(tAccuracy)), tAccuracy, '-')
                plt.title('Training accuracy - {} set {}'.format(self.options.dataset, self.options.jhmdb21set))
                pdf.savefig()  # saves the current figure into a pdf page
                plt.close()
            print("Model saved: {0}".format(savePath))
            data.resetVideo('test')
            data.loadDB('test')
            vAccuracy = []
            for idx in range(0,batch_idxsTest):
                init = int(self.options.batch_size*idx)
                end  = int(self.options.batch_size*(idx+1))
                [valiObs, valiPre] = data.getBatchRGB("test", init, end)
                valiBatch = {self.inputs: valiObs, self.labels: valiPre}
                logits = self.logits.eval(feed_dict={self.inputs:valiObs})
                tp = 0
                for i in range(self.options.batch_size):
                    if np.argmax(logits[i]) == np.argmax(valiPre[i]):
                        tp+=1
                accuVali = tp/self.options.batch_size
                vAccuracy.append(accuVali)
                print("Validation:{} [{}/{}] accuracy: {}".format(epoch, init, batch_idxsTest*self.options.batch_size, accuVali))
            meansTrain.append(np.mean(np.array(tAccuracy)))
            meansTest.append(np.mean(np.array(vAccuracy)))
            with open('mail/mail_{}.txt'.format(self.options.tbName), 'w') as the_file:
                the_file.write("Epoch Train Test\n")
                for i in range(len(meansTest)):
                    the_file.write("{} {} {}\n".format(i,meansTrain[i],meansTest[i]))
                the_file.close()

            os.system('mutt crodriguezop@gmail.com -a ./mail/loss.pdf ./mail/training_accu.pdf -s "Subject: Training {} - Epoch {}" < ./mail/mail_{}.txt'.format(self.options.tbName,epoch,self.options.tbName))

                # summary = self.sess.run(valiSummary, feed_dict={valiError: accuVali})
                # tfSummaryWriter.add_summary(summary,epoch)


                    #np.save('../npy/tAccuracy_{}_{}_{}'.format(self.options.tbName,self.options.dataset,auxl),tAccuracy)
                    #np.save('../npy/vAccuracy_{}_{}_{}'.format(self.options.tbName,self.options.dataset,auxl),vAccuracy)
                    #auxl += 1
