import os
import time
import numpy as np
import tensorflow as tf

from dbFactory import DBFactory
from modelFactory import ModelFactory

from train.train import *
from utils.utils import show_all_variables, mkdir_del

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

class TrainLSTM_DI(BaseTrain):
    def __init__(self, sess, options):
        self.sess = sess
        # self.model1 = ModelFactory.instance()
        self.options = options

        # self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.logits, labels=labels))
        # correct_prediction = tf.equal(tf.argmax(self.logits, 1), tf.argmax(labels, 1))
        # self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        # self.saver = tf.train.Saver()

    def genFeatures(self):
        modelr = ModelFactory.instance(self.options.get('models','model2'), self.options)
        inputsDI = tf.placeholder(tf.float32,
                                  [None,
                                  int(self.options.get('model2','height')),
                                  int(self.options.get('model2','width')),
                                  int(self.options.get('model2','channels'))],
                                  name='Input_DI')

        with tf.variable_scope('DI'):
            logitsDI, _  = modelr.build(inputsDI)
        tf.global_variables_initializer().run()
        loader = tf.train.Saver(var_list=modelr.variablesToLoad())
        loader.restore(self.sess, '/home/crodriguezo/projects/activity-prediction/checkpoints/renamed/DI_UCF24/ucf24_c_epoch_0_0.ckpt')

        saverDI = tf.train.Saver(modelr.variablesToLoad())
        could_load, checkpoint_counter = self.loadNET(saverDI,'/home/crodriguezo/projects/activity-prediction/checkpoints/renamed/DI_UCF24/')

        if could_load:
            counter = checkpoint_counter
            print(" [*] Load SUCCESS")
        else:
            print(" [!] Load failed...")

        data = DBFactory.instance(self.options)
        data.getFeatures(inputsDI,_['PreLogitsFlatten'], 'test', True)

    def loadNET(self, saver, checkpoint):
        import re
        import os
        print(" [*] Reading checkpoints...")
        checkpoint_dir = checkpoint
        print(checkpoint_dir)
        ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
        if ckpt and ckpt.model_checkpoint_path:
            ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
            saver.restore(self.sess, os.path.join(checkpoint_dir, ckpt_name))
            counter = int(next(re.finditer("(\d+)(?!.*\d)",ckpt_name)).group(0))
            print(" [*] Success to read {}".format(ckpt_name))
            return True, counter
        else:
            print(" [*] Failed to find a checkpoint")
            return False, 0
    def nms(self, lstm_length, belief, proposal, frames):
        aux_belief = []
        aux_mean   = []
        aux_std    = []
        ## discard all proposals where confidence < th =0.7
        for j in range(lstm_length):
            if np.sum(belief[j,:20] > 0.2) > 0:
                for z in range(20):
                    #print(proposal[i,j,:])
                    if belief[j,z] > 0.2:
                        aux_belief.append(belief[j,z])
                        aux_mean.append(proposal[j,z])
                        aux_std.append(proposal[j,z+20])

        aux_belief = np.array(aux_belief)
        aux_mean   = np.array(aux_mean)
        aux_std    = np.array(aux_std)

        ### computing init and ending
        begin  = (aux_mean - aux_std) * frames
        ending = (aux_mean + aux_std) * frames

        ### sort it out by confidence descend
        ind  = np.argsort(aux_belief)

        area = (ending - begin + 1).astype(float)

        pick = []
        while len(ind) > 0:
            ii = ind[-1]
            pick.append(ii)
            ind = ind[:-1]

            tt1 = np.maximum(begin[ii], begin[ind])
            tt2 = np.minimum(ending[ii], ending[ind])

            wh = np.maximum(0., tt2 - tt1 + 1.0)
            o = wh / (area[ii] + area[ind] - wh)
            ind = ind[np.nonzero(o <= 0.6)[0]]

        return aux_belief[pick], (aux_mean[pick] - aux_std[pick]) * frames, (aux_mean[pick] + aux_std[pick]) * frames

    def ap(self, gt, beginning, ending, iou=0.5):
        precision = []
        for i in range(len(beginning)):
            area_p = ending[i]-beginning[i]
            flag = 0
            for j in range(len(gt)):
                if flag == 1:
                    break
                tt1 = np.maximum(int(gt[j][0]), beginning[i])
                tt2 = np.minimum(int(gt[j][1]), ending[i])
                area_gt = int(gt[j][1]) - int(gt[j][0])

                wh  = np.maximum(0., tt2 - tt1 + 1.0)
                o = wh / (area_gt + area_p - wh)
                if o >= iou:
                    flag = 1

            precision.append(flag)

        total_good = np.sum(precision)
        if total_good == 0:
            return 0

        cum_sum    = np.cumsum(precision)
        ap=0
        for i in range(len(precision)):
            if precision[i] == 1:
                ap += float(cum_sum[i])/(i+1)
                # print(precision[i],(i+1), precision[i]/(i+1), ap)
        return ap/total_good
    def run(self):
        batch_size   = int(self.options.get('experiment','batch_size'))
        hidden_size  = int(self.options.get('experiment','hidden_size'))
        feature_size = int(self.options.get('experiment','feature_size'))

        self.index  = tf.placeholder(tf.int32, [None, ])
        self.input = tf.placeholder(tf.float32, [None, None, feature_size])
        self.labels = tf.placeholder(tf.float32, [None, None, 60])
        labels     = self.labels
        #
        initializer = tf.random_uniform_initializer(-1, 1)
        cell        = tf.contrib.rnn.LSTMCell(hidden_size, feature_size, initializer=initializer)
        cell_out    = tf.contrib.rnn.OutputProjectionWrapper(cell, 60)
        self.outputs, self.state  = tf.nn.dynamic_rnn(cell_out,
                                        self.input,
                                        sequence_length=self.index,
                                        dtype=tf.float32)
        output_shape = tf.shape(self.outputs)
        self.confidence = tf.nn.sigmoid(self.outputs[:,:,:20])
        self.proposal   = self.outputs[:,:,20:]

        self.loss_conf = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels = labels[:,:,:20], logits = self.outputs[:,:,:20]))
        self.loss_l2   = tf.reduce_mean(tf.abs(labels[:,:,20:] - self.proposal))  # L1
        self.loss = self.loss_conf + self.loss_l2
        self.optimizer = tf.train.AdamOptimizer(learning_rate=0.00001).minimize(self.loss)
        init = tf.initialize_all_variables()
        self.sess.run(init)

        lossSummary   = tf.summary.scalar("training loss", self.loss)
        lossConf_summ = tf.summary.scalar("sigmoid cross loss", self.loss_conf)
        lossL2_summ   = tf.summary.scalar("L2 loss", self.loss_l2)

        tfSummaryWriter = tf.summary.FileWriter("{}".format(self.options.get('experiment','tensorboard')))
        tfSummaryWriter.add_graph(self.sess.graph)
        self.saver = tf.train.Saver(tf.all_variables(), max_to_keep=int(self.options.get('experiment','epochs')))
        self.loadNET(self.saver, "checkpoints/default_adam_l1_0.00001_proposals/")
        print("Loading DB")
        data = DBFactory.instance(self.options)
        train_size, test_size = data.loadDB()
        batch_idxsTrain = train_size // batch_size
        batch_idxsTest  = test_size // batch_size
        maximum = []
        summaryCounter = 0
        for epoch in range(int(self.options.get('experiment','epochs'))):
            # for idx in range(0,batch_idxsTrain):
            #     timeStart_iter = time.time()
            #     init = int(batch_size*idx)
            #     end  = int(batch_size*(idx+1))
            #     f,la,l, frames = data.getBatch('train',init,end)
            #     # print(la[0,0,:])
            #     feeding = {self.input: f, self.index:l, self.labels: la}
            #     _, loss, l_summary, conf_summary, l2_summary = self.sess.run([self.optimizer, self.loss, lossSummary, lossConf_summ, lossL2_summ], feed_dict=feeding)
            #     print("Epoch: [{}] [{}/{}] time: {:.2f} loss: {} ".format(epoch, idx, batch_idxsTrain, time.time() - timeStart_iter, loss))
            #     summaryCounter += 1
            #     tfSummaryWriter.add_summary(l_summary,summaryCounter)
            #     tfSummaryWriter.add_summary(conf_summary,summaryCounter)
            #     tfSummaryWriter.add_summary(l2_summary,summaryCounter)
            # savePath = self.saver.save(self.sess,"{}/{}_epoch_{}.ckpt".format(self.options.get('experiment','save_ckpt_path'),self.options.get('experiment','save_ckpt_name'),epoch))
            # print(batch_idxsTest)
            final_map = []
            for idx in range(0,batch_idxsTest):

                timeStart_iter = time.time()
                init = int(batch_size*idx)
                end  = int(batch_size*(idx+1))
                f, la, l, frames = data.getBatch('vali',init,end)
                feeding = {self.input: f, self.index:l}
                belief   = self.confidence.eval(feeding)
                proposal = self.proposal.eval(feeding)

                ### computing AP
                map = []
                for i in range(32):
                    abelief, abeginning, aending = self.nms(l[i], belief[i,:,:], proposal[i,:,:], frames[i])
                    ap = self.ap(la[i], abeginning, aending)
                    map.append(ap)
                    # for j in range(len(abelief)):
                    #     print(abelief[j], abeginning[j], aending[j])
                    # print(la[i])
                final_map.append(np.mean(map))
            print(np.mean(final_map))
