#!/usr/bin/python3

class BaseTrain(object):
    def build_model(self):
        raise NotImplementedError

    def run(self, data):
        raise NotImplementedError
