#!/usr/bin/python3

import os
import sys
import params
import argparse
import numpy as np
import configparser
import tensorflow as tf

from dbFactory import DBFactory
from trainFactory import TrainFactory
# from testingFactory import TestFactory

parser = argparse.ArgumentParser(description='Config experiment file.')
parser.add_argument('--settings', dest='settings', type=str, help='Path to file with the experiments settings.')
parser.add_argument('--GPUID', dest='GPUID', type=str, help="GPU's ID that will be used in the experiment.")
parser.add_argument('--grow', dest='grow', type=int, default=1, help="(True) to not assign all GPU memory to the experiment and just grow depending on requirements.")
args = parser.parse_args()

config = configparser.ConfigParser()
if args.settings == None:
    print("###############################################")
    print("## Warning: It is necessary a settings file. ##")
    print("###############################################")
    exit()
# else:
    ## check if file exists
config.read(os.getcwd()+'/configs/'+str(args.settings))

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]=args.GPUID

run_config = tf.ConfigProto()
if args.grow == 1:
    run_config.gpu_options.allow_growth=True

with tf.Session(config=run_config) as sess:
    if config.get('experiment','stage') == 'train':
        train = TrainFactory.instance(sess, config)
        train.genFeatures()
    elif config.get('experiment','stage') == 'test':
        test  = TestFactory.instance(sess, config)
        test.run()
#     if options.state == "train":
#         if options.model2 != "None":
#             model2 = ModelFactory.instance(options,2)
#             train = TrainFactory.instance(sess, model1, options, model2)
#             train.run(data)
#         else:
#             train = TrainFactory.instance(sess, model1, options)
#             train.run(data)
#
#     elif options.state == "test":
#         if options.model2 != "None":
#             model2 = ModelFactory.instance(options,2)
#             test = TestFactory.instance(sess, model1, options, model2)
#             test.run(data)
#         else:
#             test = TestFactory.instance(sess, model1, options)
#             test.run(data)
