#!/usr/bin/python3

class BaseTest(object):
    def load(self):
        raise NotImplementedError

    def run(self, data):
        raise NotImplementedError
